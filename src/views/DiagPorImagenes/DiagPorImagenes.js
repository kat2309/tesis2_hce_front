// DiagPorImagenes
import React from "react";
import { Nav, Progress, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody, ModalHeader,Modal,InputGroup,InputGroupAddon} from 'reactstrap';
import {imprimir} from "../../utils/generales.js";

class DiagPorImagenes extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            nombres: "",
            apellidos:"",
            telefono:"",
            ocupacion:"",
            genero:"",
            domicilio:"",
            grupofactor:"",
            lugarnacimiento:"",
            provincia:"",
            distrito:"",
            idioma:"",
            // flag:{
                cancelarForm:false,
                provinciadisab:true,
                distritodisab:true,
            // }
            porcCompletado:0,
            modalDiagnosticoImagenes: false,
            examenes: []
        };
    };

    abrirModal = () => {
        this.setState({
            modalDiagnosticoImagenes: true
        })
    }

    render(){
        return(
            <div>
                <Button onClick={this.abrirModal}> Abrir Modal </Button>
                <Modal isOpen={this.state.modalDiagnosticoImagenes}>
                    <ModalHeader>
                        Agregar examen adicional
                    </ModalHeader>
                    <ModalBody>
                    <FormGroup tag="fieldset">
                        <Label> <b>Tipo de Examenes</b>  </Label>
                        <Row>
                            <Col>
                            <FormGroup check>
                            <Label check>
                                <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                    Ecografia
                    
                            </Label>
                            </FormGroup>
                            </Col>

                            <Col>
                            <FormGroup check>
                            <Label check>
                                <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                    Radiografia
                            </Label>
                            </FormGroup>
                            </Col>
                        </Row>
                    </FormGroup>

                    <FormGroup>
                        <InputGroup>
                            <Input />
                            <InputGroupAddon addonType="append"><Button>Buscar</Button></InputGroupAddon>
                        </InputGroup> 
                    </FormGroup>

                    <div>
                        <b>Examenes escogidos </b>
                        <ul>
                        <li><b> 7777</b> - Radiologia - Torax</li>
                        <li><b> 7777</b> - Radiologia - Torax</li>
                            {this.state.examenes.map(examen => {
                                return (
                                    <li><b> 7777</b> - Radiologia - Torax</li>
                                )
                            })}
                        </ul>
                    </div>
                
        
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}


export default DiagPorImagenes;