import React from "react";
import { Nav, Progress, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody,Modal,InputGroup,
    InputGroupAddon,TabContent, TabPane,Table} from 'reactstrap';
import routes from './../../routes';
import {imprimir} from "../../utils/generales.js";

class Inicio extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            esNuevo:false,
            nombres: "",
            apellidos:"",
            telefono:"",
            ocupacion:"",
            genero:"",
            domicilio:"",
            weblink:"",
            cancelarForm:false,
      
        };
    };
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        console.log(value,id);
 
        await this.setState({
          [ id ]: value,
          
        });
    }
    
    render(){
        return(
            <Card body>
                <h2 class="mb-3" align="center"> Consultar Paciente </h2>
                <p></p>
                {/* <div>
                    <img className="imagen-enfermeraWriting "  src="https://st.depositphotos.com/1037178/2882/v/950/depositphotos_28820083-stock-illustration-nurse-vector-cartoon-illustration.jpg"/>
                </div> */}
            
                <Form>
                  
                    <FormGroup>
                        <Label for= "dni" >
                            DNI: 
                        </Label>
                        {/* <InputGroup>
                            <Input id="dni" name="dni" placeholder="Completar DNI" onChange={(e)=>{this.handleChange(e)}} required/> 
                            <InputGroupAddon addonType="append"><Button>Buscar</Button></InputGroupAddon>
                        </InputGroup>  */}
                        <Col><Button onClick={this.escaneardni}  block  color="info"> <b>Escanear Dni</b>  </Button>  </Col>
                    </FormGroup>

                    <FormGroup>
                        <text> <b>Nombre Completo:  </b>  Katherine Ruiz <p> </p>  </text>
                        <text> <b> DNI: </b> 72573563 <p> </p> </text>
                        <text> <b>Edad: </b> 22 <p> </p> </text>
                        <text> <b>Grupo sanguineo:</b> O+ <p> </p> </text>
                        <text> <b>Genero:</b> Femenino <p> </p> </text>
                       
                        <p></p>
                    </FormGroup>

                    <FormGroup>
                        
                        <TabContent> 
                            <TabPane>
                                <Table bordered >
                                    <thead>
                                        <tr>
                                            <th class= "text-center" bgcolor="#eb9694" >DNI</th>
                                            <th class= "text-center" bgcolor="#eb9694" >Fecha atendida</th>
                                            <th colspan="1" class= "text-center" bgcolor="#eb9694"  >Tipo de Consulta</th>
                                            <th colspan="1" class= "text-center" bgcolor="#eb9694"  >Área de la especialización</th>
                                            <th colspan="1" class= "text-center" bgcolor="#eb9694"  > Ver detalle</th> 
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <tr>
                                            
                                            <td >
                                                <text>
                                                    72573563
                                                </text>
                                            </td>
                                            <td ><text>  23/09/2019</text> </td>
                                            <td ><text> Emergencia</text> </td>
                                            <td ><text> Dermatología</text> </td>
                                            <td ><Button color="link">Ver detalle</Button> </td>
                                            
                                        </tr>
                                    </tbody>
                                </Table>
                            </TabPane>
                        </TabContent>
                      
                    </FormGroup>

                </Form>

                <Row>
                    {this.state.esNuevo ? 
                     <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}> 
                     <Button   block  color="success" href="http://localhost:3000/#/admision"> Registrarse </Button>  </Col> 
                        : 
                         
                    <Col sm={{ size: 12}} md={{ size: 2}}> 
                    <Button onClick={this.atender}  block  color="info" href="http://localhost:3000/#/ConsultaExterna"> Atender </Button>  </Col>  
                    }   
                </Row>
           
            </Card>
        )
    }

} 

export default Inicio;