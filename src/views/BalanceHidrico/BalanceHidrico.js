import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,FormGroup, Spinner, table, Table,colgroup} from 'reactstrap';
import classnames from 'classnames';
import { async } from "q";
import Tables from "../Base/Tables/Tables";

class BalanceHidrico extends React.Component { 
    constructor(props) {
        super(props);  
        this.state = {
         
            t1valor1: 0,
            t1valor2: 0,
            t1valor3: 0,
            t1valor4:0,

            t1valor11: 0,
            t1valor12: 0,
            t1valor13: 0,
            t1valor14:0,
            t1valor15: 0,
            t1valor16: 0,
            t1valor17: 0,

            t2valor1: 0,
            t2valor2: 0,
            t2valor3: 0,
            t2valor4:0,

            t2valor21: 0,
            t2valor22: 0,
            t2valor23: 0,
            t2valor24:0,
            t2valor25: 0,
            t2valor26: 0,
            t2valor27: 0,
       

            t3valor1: 0,
            t3valor2: 0,
            t3valor3: 0,
            t3valor4:0,
            
            t3valor31: 0,
            t3valor32: 0,
            t3valor33: 0,
            t3valor34:0,
            t3valor35: 0,
            t3valor36: 0,
            t3valor37: 0,
        
            t4valor1: 0,
            t4valor2: 0,
            t4valor3: 0,
            t4valor4:0,

            t4valor41: 0,
            t4valor42: 0,
            t4valor43: 0,
            t4valor44:0,
            t4valor45: 0,
            t4valor46: 0,
            t4valor47: 0,
            familiares:[],

            ocupacion:"",
            fechahoy:"",
            
            // flag:{
                cancelarForm:false,
                provinciadisab:true,
                distritodisab:true,
            // }
            porcCompletado:0,
        };
    };
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
       
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+8.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-8.3});
        
        if(value==NaN) value=0;

        await this.setState({
          [ id ]: value,
        });
    }
    agregarFamiliar=()=>{
        this.setState({
            familiares:this.state.familiares.concat({}),
        })
    }
    componentDidMount(){
        let hoy = new Date();
        let hoyString = hoy.getDate() + "/" + (hoy.getMonth()+1)+"/"+hoy.getFullYear();
        
   
        this.setState({
            fechahoy:hoyString,

        })
        this.agregarFamiliar();
    }
 
    render(){
        return (
            <div>
            {this.state.familiares.map(familiar=>{
                return(
                <TabContent> 
                    <TabPane>
                <Table bordered >
                {/* <colgroup>
                    <col span="1" style="background-color:red"/>
                    <col style="background-color:yellow"/>
                </colgroup> */}
      
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th colspan="5" class= "text-center" bgcolor="#37c07b"  >Ingresos</th>
                        <th colspan="8" class= "text-center" bgcolor="#e83c2f"   >Egresos</th> 
                    </tr>
                </thead> 
                <thead>
                <tr>
                    <td>
                        {this.state.fechahoy}
                    </td>
                    <td bgcolor="#7bdcb5" class= "text-center" colspan="2"md={{ size: 2, offset: 5 }}>ENDOVENOSA</td>
                    <td bgcolor="#7bdcb5"  class= "text-center" colspan="2">ENTERAL</td>
                    <th bgcolor="#7bdcb5"  class= "text-center" border="0" frame="below" >TOTAL</th>
                    
                    {/* <td>name1</td>
                    <td>price1</td> */}
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">ORINA</td>
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">SNG</td> 
                    {/* sonda naso gastrica */}
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">KEHR</td>
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">FISTULA</td>
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">DRENES</td>
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">APOSITO</td>
                    {/* PUS */}
                    <td  bgcolor="#f47373" class= "text-center" colspan="1">HECES</td>
                    <th  bgcolor="#f47373" class= "text-center" colspan="1">TOTAL</th>
                </tr>
                </thead>

                <thead>
                <tr>
                    <th  class= "text-center" bgcolor="#ff6900">HORAS</th>
                    <td  class= "text-center"  bgcolor="#b1e4d0">Via Parenteral </td>
                    <td  class= "text-center"  bgcolor="#b1e4d0" >Via Central</td>
                    <td  class= "text-center"  bgcolor="#b1e4d0" >SONDA</td>
                    <td  class= "text-center"  bgcolor="#b1e4d0" >Via Oral</td>
                    <td bgcolor="#7bdcb5" />

                    <td bgcolor="#f69393" class= "text-center" border="1" > </td>
                    <td bgcolor="#f69393" class= "text-center" > </td>
                    <td bgcolor="#f69393" class= "text-center" >VON</td>
                    {/* vomito */}
                    <td bgcolor="#f69393" class= "text-center" ></td>
                    <td bgcolor="#f69393" class= "text-center" >Intestinal</td>
                    <td bgcolor="#f69393" class= "text-center" >Tubular</td>
                    <td bgcolor="#f69393" class= "text-center" >Pen-R</td>
                    <td bgcolor="#f69393" class= "text-center" >Ost</td>
                    
{/* kehr sonda  */}
{/* aposito */}
                </tr>
                </thead>
               
                <tbody>
                    <tr>
                        <th scope="row"  class= "text-center" bgcolor="FF7F33" >07-13 horas</th>
                        <td ><Input id="t1valor1"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor2"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor3"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor4"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td > {(parseFloat(this.state.t1valor1|| 0 ) + parseFloat(this.state.t1valor2|| 0 )  +parseFloat(this.state.t1valor3|| 0 )  + parseFloat(this.state.t1valor4|| 0 ))||0 } </td>

                        

                        <td ><Input id="t1valor11"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor12"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor13"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor14"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor15"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor16"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t1valor17"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td > {(parseFloat(this.state.t1valor11|| 0) + parseFloat(this.state.t1valor12|| 0)+parseFloat(this.state.t1valor13|| 0)+parseFloat(this.state.t1valor14|| 0)+
                                     parseFloat(this.state.t1valor15|| 0)+parseFloat(this.state.t1valor16|| 0)+parseFloat(this.state.t1valor17|| 0))||0} </td>

                    </tr>
                    <tr>
                        <th scope="row"  class= "text-center" bgcolor="FF7F33">13-19 hrs</th>
                        <td ><Input id="t2valor1"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor2"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor3"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor4"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{(parseFloat(this.state.t2valor1|| 0) + parseFloat(this.state.t2valor2|| 0)+parseFloat(this.state.t2valor3|| 0)+parseFloat(this.state.t2valor4|| 0))||0 } </td>

                        <td ><Input id="t2valor21"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor22"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor23"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor24"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor25"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor26"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t2valor27"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{parseFloat(this.state.t2valor21 || 0) + parseFloat(this.state.t2valor22 || 0)+parseFloat(this.state.t2valor23 || 0)+parseFloat(this.state.t2valor24 || 0)+
                         parseFloat(this.state.t2valor25 || 0)+parseFloat(this.state.t2valor26 || 0)+parseFloat(this.state.t2valor27 || 0) } </td>

                    </tr>
                    <tr>
                        <th scope="row" class= "text-center" bgcolor="FF7F33">19-01 hrs</th>
                        <td ><Input id="t3valor1"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor2"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor3"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor4"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{parseFloat(this.state.t3valor1 || 0 ) + parseFloat(this.state.t3valor2 || 0 )+parseFloat(this.state.t3valor3 || 0 )+parseFloat(this.state.t3valor4 || 0 ) } </td>

                        <td ><Input id="t3valor31"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor32"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor33"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor34"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor35"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor36"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t3valor37"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{parseFloat(this.state.t3valor31|| 0) + parseFloat(this.state.t3valor32|| 0)+parseFloat(this.state.t3valor33|| 0)+parseFloat(this.state.t3valor34|| 0)+
                                parseFloat(this.state.t3valor35|| 0) + parseFloat(this.state.t3valor36|| 0)+parseFloat(this.state.t3valor37|| 0)} </td>

                    </tr>

                    <tr>
                        <th scope="row" class= "text-center" bgcolor="FF7F33">01-07 hrs</th>
                        <td ><Input id="t4valor1"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor2"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor3"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor4"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{parseFloat(this.state.t4valor1|| 0) + parseFloat(this.state.t4valor2|| 0)+parseFloat(this.state.t4valor3|| 0)+parseFloat(this.state.t4valor4|| 0) } </td>

                        
                        <td ><Input id="t4valor41"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor42"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor43"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor44"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor45"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor46"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <td >{parseFloat(this.state.t4valor41|| 0) + parseFloat(this.state.t4valor42|| 0)+parseFloat(this.state.t4valor43|| 0)+parseFloat(this.state.t4valor44|| 0)+
                        parseFloat(this.state.t4valor45|| 0) + parseFloat(this.state.t4valor46|| 0)+parseFloat(this.state.t4valor47|| 0) } </td>

                    </tr>

                    <tr>
                        <th>TOTAL</th>
                        <td > {parseFloat(this.state.t1valor1|| 0) + parseFloat(this.state.t2valor1|| 0)+parseFloat(this.state.t3valor1|| 0)+parseFloat(this.state.t4valor1|| 0) } </td>
                        <td > {parseFloat(this.state.t1valor2|| 0) + parseFloat(this.state.t2valor2|| 0)+parseFloat(this.state.t3valor2|| 0)+parseFloat(this.state.t4valor2|| 0) } </td>
                        <td > {parseFloat(this.state.t1valor3|| 0) + parseFloat(this.state.t2valor3|| 0)+parseFloat(this.state.t3valor3|| 0)+parseFloat(this.state.t4valor3|| 0) } </td>
                        <td > {parseFloat(this.state.t1valor4|| 0) + parseFloat(this.state.t2valor4|| 0)+parseFloat(this.state.t3valor4|| 0)+parseFloat(this.state.t4valor4|| 0) } </td>
                            
                        <td > {parseFloat(this.state.t1valor4|| 0) + parseFloat(this.state.t2valor4|| 0)+parseFloat(this.state.t3valor4|| 0)+parseFloat(this.state.t4valor4|| 0)+
                                parseFloat(this.state.t1valor3|| 0) + parseFloat(this.state.t2valor3|| 0)+parseFloat(this.state.t3valor3|| 0)+parseFloat(this.state.t4valor3|| 0)+
                                parseFloat(this.state.t1valor2|| 0) + parseFloat(this.state.t2valor2|| 0)+parseFloat(this.state.t3valor2|| 0)+parseFloat(this.state.t4valor2|| 0)+
                                parseFloat(this.state.t1valor1|| 0) + parseFloat(this.state.t2valor1|| 0)+parseFloat(this.state.t3valor1|| 0)+parseFloat(this.state.t4valor1|| 0)
                         } </td>
                            
                        <td > {parseFloat(this.state.t1valor11 || 0) + parseFloat(this.state.t2valor21 || 0)+parseFloat(this.state.t3valor31 || 0)+parseFloat(this.state.t4valor41 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor12 || 0) + parseFloat(this.state.t2valor22 || 0)+parseFloat(this.state.t3valor32 || 0)+parseFloat(this.state.t4valor42 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor13 || 0) + parseFloat(this.state.t2valor23 || 0)+parseFloat(this.state.t3valor33 || 0)+parseFloat(this.state.t4valor43 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor14 || 0) + parseFloat(this.state.t2valor24 || 0)+parseFloat(this.state.t3valor34 || 0)+parseFloat(this.state.t4valor44 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor15 || 0) + parseFloat(this.state.t2valor25 || 0)+parseFloat(this.state.t3valor35 || 0)+parseFloat(this.state.t4valor45 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor16 || 0) + parseFloat(this.state.t2valor26 || 0)+parseFloat(this.state.t3valor36 || 0)+parseFloat(this.state.t4valor46 || 0) } </td>
                        <td > {parseFloat(this.state.t1valor17 || 0) + parseFloat(this.state.t2valor27 || 0)+parseFloat(this.state.t3valor37 || 0)+parseFloat(this.state.t4valor47 || 0) } </td>
                    
                        <td>{parseFloat(this.state.t1valor11 || 0) + parseFloat(this.state.t2valor21 || 0)+parseFloat(this.state.t3valor31 || 0)+parseFloat(this.state.t4valor41 || 0) +
                            parseFloat(this.state.t1valor12 || 0) + parseFloat(this.state.t2valor22 || 0)+parseFloat(this.state.t3valor32 || 0)+parseFloat(this.state.t4valor42 || 0) +
                            parseFloat(this.state.t1valor13 || 0) + parseFloat(this.state.t2valor23 || 0)+parseFloat(this.state.t3valor33 || 0)+parseFloat(this.state.t4valor43 || 0) +
                            parseFloat(this.state.t1valor14 || 0) + parseFloat(this.state.t2valor24 || 0)+parseFloat(this.state.t3valor34 || 0)+parseFloat(this.state.t4valor44 || 0) +
                            parseFloat(this.state.t1valor15 || 0) + parseFloat(this.state.t2valor25 || 0)+parseFloat(this.state.t3valor35 || 0)+parseFloat(this.state.t4valor45 || 0) +
                            parseFloat(this.state.t1valor16 || 0) + parseFloat(this.state.t2valor26 || 0)+parseFloat(this.state.t3valor36 || 0)+parseFloat(this.state.t4valor46 || 0) +
                            parseFloat(this.state.t1valor17 || 0) + parseFloat(this.state.t2valor27 || 0)+parseFloat(this.state.t3valor37 || 0)+parseFloat(this.state.t4valor47 || 0)}</td>
                    </tr>
                </tbody>
                {/* <p></p>
                <tr></tr> */}          
                <thead>
                    <tr>
                        <th colspan="4" >INGRESOS EN 24HR</th>
                        <td colspan="2"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                        <th colspan="3" >Balance en 24HR</th>
                        <th colspan="4" >Egreso en 24HR</th>
                        <td colspan="1"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    </tr>
                    <tr>
                    <th colspan="4" class= "text-center" bgcolor="#37c07b"  >Agua metabolica</th>   
                    <td colspan="2" ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    <td colspan="3" rowspan="2"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    <th colspan="4" >Perdida sin sensibles</th>
                    <td colspan="1"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    </tr>
                    <tr>
                    <th colspan="4" class= "text-center" bgcolor="#e83c2f"   >Total Ingresos</th>
                    <td colspan="2"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    {/* <td colspan="3"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td> */}
                    <th colspan="4" >Total Egresos</th>
                    <td colspan="1"  ><Input id="t4valor47"   type="text" onChange={(e)=>{this.handleChange(e)}}/> </td>
                    </tr>
                </thead>

            </Table>

            </TabPane>
                
                </TabContent>
                )
            })}
            
            <Button onClick={this.agregarFamiliar}>Agregar Nueva Tabla</Button>
            </div>
     

            )};
} 

export default BalanceHidrico;

// <table>
//             <thead>
//                 <tr>
//                     <th colspan="2">The table header</th>
//                 </tr>
//             </thead>
//             <tbody>
//                 <tr>
//                     <td>The table body</td>
//                     <td>with two columns</td>
//                 </tr>
//             </tbody>
//             </table>