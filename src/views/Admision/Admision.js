import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,Form,
    DropdownItem,FormGroup, Table, ModalHeader,InputGroupAddon} from 'reactstrap';
    import classnames from 'classnames';
import {imprimir} from "../../utils/generales.js";
import fondo from '../../assets/img/brand/fondoHos.jpg'

class Admision extends React.Component {
    constructor(props) {
        super(props);
 
        this.state = {

            nombreEnfermedad:"",
            tiempoEnfermedad:"",
            signosysintPrincipales:"",
            relatocronologico:"",
            funcionesBiologicas:"",
            material:"",
            numPersonas:0,
            numHabitaciones:0,
            trabajo:"",
            condicionsocioecon:"",
            viajesRecientes:"",
            prenatal:"",
            parto:"",
            atendidoen:"",
            lactanciaMat:"",
            ablactancia:"",
            desarrollopsicomotriz:"",
            menarquia:"",
            inicioRelaSex:"",
            regimencatamenal:"",
            usoanticonceptivo:"",
            ultimoparto:"",
            porcCompletado:0,
            activeTab: '1',
            inmunizaciones:[],
            cancelarForm:false,
            dropdownOpen: false,
            splitButtonOpen: false,
            tipFecha:"dia",
         
        };
        this.toggleDropDown = this.toggleDropDown.bind(this);

    }

    agregarInmunizacion=()=>{
        this.setState({
            inmunizaciones:this.state.inmunizaciones.concat({}),
        })
    }

    toggle=(tab)=> {
        // console.log("el tab es: ", tab)
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab,
            
          });
        }
    }

    handleChange = async (event) => {
        const { target } = event;
        console.log(event);
        const { value,id } = target;
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+2});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-2});

        await this.setState({
          [ id ]: value,
          //dropdownOpen:true,
        });
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    toggleDropDown = async (e) => {
        const { target } = e;
        const { value,id } = target;
        await this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
        if (id===""||id===null || id===undefined || id==NaN) return;   
        await this.setState({
            tipFecha:id,
        });
         console.log(id);
        // await this.setState({
        //     dropdownOpen:value,
        // });
      }
    
      selecccionarTipFecha = (tipo) => {
          console.log(tipo);
      }

      tipoFechaValorATexto = (valor) => {
        if(valor==="dia") 
            return "dia";
        else if(valor==="mes") 
            return "mes";
        else return "anhio"
      }
      continuaryGuardar=()=>{
          console.log(parseInt(this.state.activeTab));
        this.setState({
            activeTab: ( parseInt(this.state.activeTab)+1).toString()
          });
      }
    render(){
        return (

            <Card body className="bfondo-hos">
                
                <h2 class="mb-3" align="center"> Información Personal </h2>
                <p></p>
                <div>
                    <img className="imagen-enfermeraWriting "  src="https://st.depositphotos.com/1037178/2882/v/950/depositphotos_28820083-stock-illustration-nurse-vector-cartoon-illustration.jpg"/>
                </div>
                <div class="text-center" >{this.state.porcCompletado}%</div>
                <Progress width="10px" sm={{ size: 4}} md={{ size: 4, offset: 0 }} value={this.state.porcCompletado} /> 
                <Nav tabs>

                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }} >
                    Parte I
                    {/* <h1 style="color:rgb(255,0,0)">Heading</h1> */}
                </NavLink>
                </NavItem>
                
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '2' })}  onClick={() => { this.toggle('2'); }} >Parte II</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >Parte III</NavLink>
                </NavItem>
 
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1"  className="fond">
                    {/* <Badge color="primary">Primary</Badge> */}
                    <Form>
                    <FormGroup>
                        <Label for= "dni" >
                            DNI: 
                        </Label>
                        <text> 72573563</text>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "nombres" >
                            Nombres: 
                        </Label>
                        <text> Katherine Inés</text>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "apellidos">
                            Apellidos: 
                        </Label>
                        <text> Ruiz Quispe</text>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "telefono" >
                            Teléfono: 
                        </Label>
                        <Input id="telefono" placeholder="Telefono" maxlength="9" onChange={(e)=>{this.handleChange(e)}}/> 
                    </FormGroup>
                    <FormGroup>
                        <Label for= "ocupacion">
                            Ocupación: 
                        </Label>
                        <Input id="ocupacion" placeholder="Ocupacion"  onChange={(e)=>{this.handleChange(e)}}/> 
                    </FormGroup>
                     <Row>
                        <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                        <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuaryGuardar}   block  color="warning"> Continuar</Button> </Col>
                    </Row>
                    </Form>
                    <p></p>
                </TabPane>

                <TabPane tabId="2" className="fond">
                <Form>
                <FormGroup>
                        <Label for= "fechanacimiento">
                            Fecha de Nacimiento: 
                        </Label>
                        <text> 23/09/1996</text>
                        {/* <Input id="fechanacimiento"   type="date" onChange={(e)=>{this.handleChange(e)}}/>  */}
                    
                    </FormGroup>
                    <FormGroup>
                        <Label for= "genero">
                            Género: 
                        </Label>
                        <text> Femenino</text>
                        {/* <Input type="select" name="select"  placeholder="Genero"  id="genero" onChange={(e)=>{this.handleChange(e)}} >
                            <option> Femenino</option>
                            <option> Masculino</option>
                        </Input> */}

                    </FormGroup>
                    <FormGroup>
                        <Label for= "domicilio">
                            Domicilio: 
                        </Label>
                        {/* <Input  id="domicilio" placeholder="Domicilio" onChange={(e)=>{this.handleChange(e)}} /> */}
                        <text> Calle Los Rosales  123</text>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "grupofactor">
                            Grupo y Factor: 
                        </Label>
                        <Input type="select" name="select"  placeholder="Grupo y Factor" id="grupofactor" onChange={(e)=>{this.handleChange(e)}} >
                            <option> Seleccionar un grupo y factor</option>
                            <option> A</option>
                            <option> B</option>
                            <option> AB</option>
                            <option> O</option>
                        </Input>
                
                    </FormGroup>
                    <Row>
                        <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                        <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuaryGuardar}   block  color="warning"> Continuar</Button> </Col>
                    </Row>
                    </Form>
                </TabPane>

                <TabPane tabId="3" className="fond">
                <Form>
                <FormGroup>
                        <Label for= "lugarnacimiento">
                            Lugar Nacimiento: 
                        </Label>
                        <Input type="select" name="select" placeholder="Lugar Nacimiento:"id="lugarnacimiento" onChange={(e)=>{this.handleChange(e)}}  >
                            <option> Seleccionar un departamento</option>
                            <option> Lima</option>
                            <option> Tacna</option>
                            <option> Ica</option>
                            <option> Loreto</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "provincia">
                            Provincia: 
                        </Label>
                        <Input type="select" name="provincia" placeholder="Provincia" id="provincia" disabled={this.state.provinciadisab} onChange={(e)=>{this.handleChange(e)}}  >
                            <option> Seleccionar una provincia</option>
                            <option> Lima</option>
                            <option> Tacna</option>
                            <option> Ica</option>
                            <option> Loreto</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "distrito">
                            Distrito: 
                        </Label>
                        <Input type="select" name="select"placeholder="Distrito" id="distrito" disabled={this.state.distritodisab}  onChange={(e)=>{this.handleChange(e)}}  >
                        <option> Seleccionar un distrito</option>
                            <option> Lima</option>
                    
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for= "idioma">
                            Idioma: 
                        </Label>
                        <Input  type="select" name="select" id="idioma" placeholder="Idioma" disabled={this.state.idioma}  onChange={(e)=>{this.handleChange(e)}}>
                            <option> Seleccionar un idioma</option>
                            <option> Español</option>
                            <option> Inglés</option>
                            <option> Quechua</option>
                        </Input>
                    </FormGroup>

                    <Row>
                        <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                        <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarcancelarFormulario}  block  color="success"> Terminar y Guardar </Button>  </Col>  
                    </Row>
                </Form>
                </TabPane>
               
            </TabContent>
             <p></p>           
               
                <Modal centered isOpen = {this.state.cancelarForm}>
                    <ModalBody>
                        Se cancelo satisfactoriamente :)
                    </ModalBody>
                </Modal>
            </Card>
            
        );
    }
} 

export default Admision;