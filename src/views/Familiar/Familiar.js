import React from "react";
import { Nav, NavItem, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody,Modal} from 'reactstrap';
// import fondo from '../../assets/img/brand/hospitalPerf.jpg'
class Familiar extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            nombres:"",
            apellidos:"",
            genero:"",
            dni:"",
            fechanacimiento:"",
            parentesco:"",
            estadocivil:"",
            gradoinst:"",
            ocupacion:"",
            condicionocupacion:"",
            familiares:[],
            porcCompletado:0,
        };
    };
    agregarFamiliar=()=>{
        this.setState({
            familiares:this.state.familiares.concat({}),
        })
    }
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        if (id==="lugarnacimiento" && value != null){
            await this.setState({
                provinciadisab:false,
            });
        }else if(id==="provincia" && value != null){
            await this.setState({
                distritodisab:false,
            });
        }
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+8.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-8.3});
        
        await this.setState({
          [ id ]: value,
        });
    }
    render(){
        return (
            <Card body >
            <div>
               <h2 align="center"> Ficha familiar </h2>
               {this.state.familiares.map(familiar=>{
                   return(
                    <Card body >   
                    {/* className="fondo-hospital"> */}

                    <Row>
                        <Col xs="12" md="6" xl="6">
                            
                        {/* <hr className="mt-0" /> */}
                      
                            <FormGroup>
                            <Label for= "nombres" >
                                Nombres: 
                            </Label>
                            <Input id="nombres" name="nombres" placeholder="Nombres" onChange={(e)=>{this.handleChange(e)}} required/> 
                            </FormGroup>
                            <FormGroup>
                                <Label for= "apellidos">
                                    Apellidos: 
                                </Label>
                                <Input id="apellidos" placeholder="Apellidos" onChange={(e)=>{this.handleChange(e)}} required/> 
                            </FormGroup>
                            <FormGroup>
                                <Label for= "DNI">
                                    DNI: 
                                </Label>
                                <Input id="dni" placeholder="dni" onChange={(e)=>{this.handleChange(e)}} required/> 
                            </FormGroup>
                            <FormGroup>
                                <Label for= "fechanacimiento">
                                    Fecha de Nacimiento: 
                                </Label>
                                <Input id="fechanacimiento"   type="date" onChange={(e)=>{this.handleChange(e)}}/> 
                            </FormGroup>

                            <FormGroup>
                                <Label for= "genero">
                                    Genero: 
                                </Label>
                                <Input type="select" name="select"  placeholder="Genero"  id="genero" onChange={(e)=>{this.handleChange(e)}} >
                                    <option> Femenino</option>
                                    <option> Masculino</option>
                                </Input>

                            </FormGroup>
                        </Col>
                        <Col xs="12" md="6" xl="6">

                            {/* <hr className="mt-0" /> */}
                            <FormGroup>
                            <Label for= "parentesco">
                                Parentesco: 
                            </Label>
                            <Input type="select" name="select"  placeholder="parentesco"  id="parentesco" onChange={(e)=>{this.handleChange(e)}} >
                                <option> Padre</option>
                                <option> Madre</option>
                                <option> Hijo(a)</option>
                                <option> Hijo(a) Adoptivo(a)</option>
                                <option> Abuelo(a)</option>
                                <option> Tio(a)</option>
                                <option> Nieto(a)</option>
                                <option> Padrastro</option>
                                <option> Madrastra</option>
                                <option> Sobrino(a)</option>
                                <option> Primo(a)</option>
                                <option> Bisabuelo(a)</option>
                                <option> Amigo(a)</option>
                                <option> Hermano(a)</option>
                                <option> Yerno</option>
                                <option> Nuera</option>
                            </Input>
                        </FormGroup>

                        <FormGroup>
                            <Label for= "estadocivil">
                                Estado Civil: 
                            </Label>
                            <Input type="select" name="select"  placeholder="estadocivil"  id="estadocivil" onChange={(e)=>{this.handleChange(e)}} >
                                <option> Soltero</option>
                                <option> Conviviente</option>
                                <option> Casado</option>
                                <option> Separado</option>
                                <option> Divorciado</option>
                                <option> Viudo</option>
                                <option> Otros</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for= "gradoinst">
                                Grado de instruccion: 
                            </Label>
                            <Input type="select" name="select"  placeholder="gradoinst"  id="gradoinst" onChange={(e)=>{this.handleChange(e)}} >
                                <option> Sin instruccion</option>
                                <option> Inicial</option>
                                <option> Primaria completa</option>
                                <option> Primaria incompleta</option>
                                <option> Superior completo</option>
                                <option> Superior incompleto</option>
                            
                            </Input>
                        </FormGroup>

                        <FormGroup>
                            <Label for= "condicionocupacion">
                                Ocupacion: 
                            </Label>
                            <Input type="select" name="select"  placeholder="condicionocupacion"  id="condicionocupacion" onChange={(e)=>{this.handleChange(e)}} >
                                <option> Trabajador estable</option>
                                <option> Eventual</option>
                                <option> Sin ocupacion</option>
                                <option> Jubilado</option>
                                <option> Estudiante</option>
                                                    
                            </Input>
                        </FormGroup>
                        <br></br>
                        <br></br>
                        <Row>
                        <Col sm={{ size: 12}} md={{ size: 4, offset: 4 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                        <Col sm={{ size: 12}} md={{ size: 4}}>  <Button onClick={this.cancelarFormulario} block   color="success"> Guardar </Button>  </Col>
                        </Row>
                        </Col>
                    </Row>
                    
                   
                    </Card>
                   )
               })}
               <button onClick={this.agregarFamiliar}>Agregar Familiar</button>
           </div>
           </Card>
        );
    }
} 

export default Familiar;