import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card,CardBody , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,FormGroup, Spinner,
    ModalHeader,InputGroupAddon} from 'reactstrap';
import classnames from 'classnames';

class OrdenInterQuirurgica extends React.Component { 
    constructor(props) {
        super(props);  
        this.state = {
            nombres:"",
            apellidos:"",
            genero:"",
            dni:"",
            fechanacimiento:"",
            parentesco:"",
            estadocivil:"",
            gradoinst:"",
            ocupacion:"",
            condicionocupacion:"",
            familiares:[],
            porcCompletado:0,
            cancelarForm:false,
            modalDiagnosticoImagenes: false,
        };
    };
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        console.log(value,id);
        if (id==="lugarnacimiento" && value != null){
            await this.setState({
                provinciadisab:false,
            });
        }else if(id==="provincia" && value != null){
            await this.setState({
                distritodisab:false,
            });
        }
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado>91?this.state.porcCompletado=100: this.state.porcCompletado+8.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-8.3});
        
        await this.setState({
          [ id ]: value,
        });
    }
    cancelarFormulario = async()=> {
     
 
        await this.setState({
            cancelarForm:true,
         //    flag: { ...this.state.flag, cancelarForm: true}
        });
        setTimeout(()=>this.setState({
            cancelarForm:false,
        }), 3000)
       
    }
    abrirModal = () => {
        this.setState({
            modalDiagnosticoImagenes: true
        })
    }

    render(){
        return (
            <Card body>
            <h2 align="center">Orden de Intervencion Quirurgica</h2>
            <p></p>
                <div>
           
                
                <text> Nombres: <p> </p>  </text>
                <text> Apellidos:  <p> </p> </text>
                <text> DNI:  <p> </p> </text>
                <text> Edad:  <p> </p> </text>
                <text> Grupo sanguineo:  <p> </p> </text>
                <text> Direccion :  <p> </p> </text>
                <text> Diagnostico :  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; CIE-10 </text>
                <p></p>
                <Label for= "operacion" >
                    Operacion: 
                </Label>
                <Input id="operacion" name="operacion" placeholder="Coloque el nombre de la operacion que se le va a realizar" onChange={(e)=>{this.handleChange(e)}} required/> 
               

                <Label for= "diaOperacion " >
                    Dia de la operacion: 
                </Label>
                <Input id="diaOperacion " type="date" name="diaOperacion " placeholder="Coloque el dia de la diaOperacion  que se le va a realizar" onChange={(e)=>{this.handleChange(e)}} required/> 
                
                <Label for= "anestesia " >
                    Anestesia: 
                </Label>
                <Input id="anestesia "  name="anestesia " placeholder="Coloque el nombre de la anestesia" onChange={(e)=>{this.handleChange(e)}} required/> 
                
                <Label for= "cirujano " >
                    Cirujano: 
                </Label>
                <p></p>
                <Button onClick={this.abrirModal}> Busqueda de cirujanos </Button>
                
                <Modal isOpen={this.state.modalDiagnosticoImagenes}>
                    <ModalHeader>
                        Busqueda de cirujanos
                    </ModalHeader>
                    <ModalBody>
                    <FormGroup tag="fieldset">
                        <Label > En el siguiente cuadro proceda a buscar por nombre o apellido el(los) cirjuano(s) a operar </Label>
                        
                    </FormGroup>

                    <FormGroup>
                        <InputGroup>
                            <Input />
                            <InputGroupAddon addonType="append"><Button>Buscar</Button></InputGroupAddon>
                        </InputGroup> 
                    </FormGroup>

                    <div>
                        <b>Lista de cirujanos escogidos</b>
                        <ul>
                        
                            
                        </ul>
                    </div>
                
                    <Col sm={{ size: 12}} md={{ size: 4, offset: 3 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Guardar </Button>  </Col>
                    </ModalBody>
                </Modal>
                <p></p>
                {/* <Input id="cirujano "  name="cirujano " placeholder="Coloque el nombre del cirujano" onChange={(e)=>{this.handleChange(e)}} required/>  */}
                <Label for= "ayudantes " >
                    Ayudantes: 
                </Label>  
                
                <p></p>
                <Label for= "observaciones " >
                    Observaciones: 
                </Label>
                <Input id="observaciones "  name="observaciones " placeholder="Coloque las observaciones" onChange={(e)=>{this.handleChange(e)}} required/> 
                    

                </div>
                <Row>
                <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                {/* <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning"> Continuar sin guardar </Button> </Col> */}
                <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarcancelarFormulario}  block  color="success"> Guardar </Button>  </Col>  
                </Row>
                <Modal centered isOpen = {this.state.cancelarForm}>
                    <ModalBody>
                        Se cancelo satisfactoriamente :)
                    </ModalBody>
                </Modal>
            </Card>
            
        );
    }
} 

export default OrdenInterQuirurgica;