// ConsultaExterna
import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,FormGroup, Table, ModalHeader,InputGroupAddon} from 'reactstrap';
import classnames from 'classnames';

class ConsultaExterna extends React.Component { 
    // falta agregar la cama en la que se encuentra
    
    constructor(props) {
        super(props);
 
        this.state = {

            nombreEnfermedad:"",
            tiempoEnfermedad:"",
            signosysintPrincipales:"",
            relatocronologico:"",
            funcionesBiologicas:"",
            material:"",
            numPersonas:0,
            numHabitaciones:0,
            trabajo:"",
            condicionsocioecon:"",
            viajesRecientes:"",
            prenatal:"",
            parto:"",
            atendidoen:"",
            lactanciaMat:"",
            ablactancia:"",
            desarrollopsicomotriz:"",
            menarquia:"",
            inicioRelaSex:"",
            regimencatamenal:"",
            usoanticonceptivo:"",
            ultimoparto:"",
            porcCompletado:0,
            activeTab: '1',
            inmunizaciones:[],
            cancelarForm:false,
            dropdownOpen: false,
            splitButtonOpen: false,
            tipFecha:"dia",
         
        };
        this.toggleDropDown = this.toggleDropDown.bind(this);

    }

    agregarInmunizacion=()=>{
        this.setState({
            inmunizaciones:this.state.inmunizaciones.concat({}),
        })
    }

    toggle=(tab)=> {
        // console.log("el tab es: ", tab)
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }

    handleChange = async (event) => {
        const { target } = event;
        console.log(event);
        const { value,id } = target;
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+2});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-2});

        await this.setState({
          [ id ]: value,
          //dropdownOpen:true,
        });
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    toggleDropDown = async (e) => {
        const { target } = e;
        const { value,id } = target;
        await this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
        if (id===""||id===null || id===undefined || id==NaN) return;   
        await this.setState({
            tipFecha:id,
        });
         console.log(id);
        // await this.setState({
        //     dropdownOpen:value,
        // });
      }
    
      selecccionarTipFecha = (tipo) => {
          console.log(tipo);
      }

      tipoFechaValorATexto = (valor) => {
        if(valor==="dia") 
            return "dia";
        else if(valor==="mes") 
            return "mes";
        else return "anhio"
      }
      continuaryGuardar=()=>{
          console.log(parseInt(this.state.activeTab));
        this.setState({
            activeTab: ( parseInt(this.state.activeTab)+1).toString()
          });
      }
    render(){
        return (
            <Card>
                <Nav tabs>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }}  active>VACAM</NavLink>
                </NavItem>
                
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >Enfermedad Actual</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >Antecedentes</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.toggle('4'); }} > Examen fisico</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.toggle('5'); }} > Diagnostico</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '6' })} onClick={() => { this.toggle('6'); }} > Plan de trabajo</NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                    {/* <Badge color="primary">Primary</Badge> */}
                     <Label  color="success" for= "nombreEnfermedad" >
                        Enfermedad actual del paciente: 
                    </Label>
                    <Input id="nombreEnfermedad" name="nombreEnfermedad" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "tiempoEnfermedad" >
                        Tiempo de enfermedad: 
                    </Label>

                    <InputGroup>
                    <Input />
                    <InputGroupButtonDropdown toggle={this.toggleDropDown}  addonType="append" isOpen={this.state.dropdownOpen} onChange={(e)=>{this.handleChange(e)}} >
                        <DropdownToggle caret>
                            {this.tipoFechaValorATexto(this.state.tipFecha)}
                        </DropdownToggle>
                        <DropdownMenu>
                        <DropdownItem id="dia">día(s)</DropdownItem>
                        <DropdownItem id="mes"> 
                                mes(es),
                        </DropdownItem>
                        <DropdownItem id="anhio" > año(s)</DropdownItem>
                        </DropdownMenu>
                    </InputGroupButtonDropdown>
                    </InputGroup>
                    {/* <Input id="tiempoEnfermedad" name="tiempoEnfermedad" onChange={(e)=>{this.handleChange(e)}}/>  */}

                    <Label for= "signosysintPrincipales" >
                        Signos y sintomas principales: 
                    </Label>
                    <Input id="signosysintPrincipales" name="signosysintPrincipales" onChange={(e)=>{this.handleChange(e)}}/> 

                    <FormGroup>
                        <Label for="relatocronologico">Relato cronologico: </Label>
                        <Input type="textarea" name="relatocronologico" id="relatocronologico" onChange={(e)=>{this.handleChange(e)}} />
                    </FormGroup>
                    <FormGroup></FormGroup>


                    <Label for= "funcionesBiologicas" >
                        Funciones Biologicas: 
                    </Label>
                    <Input id="funcionesBiologicas" name="funcionesBiologicas" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Row>
                    <Col sm={{ size: 12}} md={{ size: 2, offset: 5 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                    <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning">Editar </Button> </Col>
                    <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuaryGuardar}  block  color="success"> Guardar y Continuar </Button>  </Col>  
                    </Row>
                    <Modal centered isOpen = {this.state.cancelarForm}>
                    <ModalBody>
                        Se cancelo satisfactoriamente :)
                    </ModalBody>
                    </Modal>
                </TabPane>

                <TabPane tabId="2">
                <h1>Vivienda</h1>
                <Label for= "material" >
                    Material
                </Label>
                <FormGroup check>
                    <Label check>
                    <Input type="radio" name="radio1" />{' '}
                        Noble
                    </Label>
                </FormGroup>

                <FormGroup check>
                    <Label check>
                    <Input type="radio" name="radio2" />{' '}
                        Precario
                    </Label>
                </FormGroup>

                <FormGroup check>
                    <Label check>
                    <Input type="radio" name="radio2" />{' '}
                        Mixto
                    </Label>
                </FormGroup>
                <p></p>
                {/* <Input id="material" name="material" onChange={(e)=>{this.handleChange(e)}}/>  */}

                <FormGroup>
                    <Label for="numPersonas">Numero de personas: </Label>
                        <Input
                            type="number"
                            name="numPersonas"
                            id="numPersonas"
                            placeholder="0"
                            onChange={(e)=>{this.handleChange(e)}}
                        />
                </FormGroup>
                
                <FormGroup>
                    <Label for="numHabitaciones">Numero de Habitaciones:  </Label>
                        <Input
                            type="number"
                            name="numHabitaciones"
                            id="numHabitaciones"
                            placeholder="0"
                            onChange={(e)=>{this.handleChange(e)}}
                        />
                </FormGroup>
               
                <Label for= "trabajo" >
                    Trabajo: 
                </Label>
                <Input id="trabajo" name="trabajo" placeholder="Trabajo" onChange={(e)=>{this.handleChange(e)}}/> 

                <Label for= "condicionsocioecon" >
                    Condicion socio economica: 
                </Label>
                <Input id="condicionsocioecon" name="condicionsocioecon"  onChange={(e)=>{this.handleChange(e)}}/> 

                <Label for= "viajesRecientes" >
                    Viajes Recientes: 
                </Label>
                <Input id="viajesRecientes" name="viajesRecientes" onChange={(e)=>{this.handleChange(e)}}/> 
                <Row>
                    <Col sm={{ size: 12}} md={{ size: 2, offset: 5 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                    <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning">Editar </Button> </Col>
                    <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuaryGuardar}  block  color="success"> Guardar y Continuar </Button>  </Col> 
                </Row>
                </TabPane>


                <TabPane tabId="3">
                    <h1>Antecedentes Fisiologicos</h1>
                    <Label for= "prenatal" >
                        Prenatal
                    </Label>
                    <Input id="prenatal" name="prenatal" onChange={(e)=>{this.handleChange(e)}}/> 
                    <h4>Natal</h4>
                    <Label for= "parto" >
                        Parto: 
                    </Label>
                    <Input id="parto" name="parto" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "atendidoen" >
                        Atendido en: 
                    </Label>
                    <Input id="atendidoen" name="atendidoen" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "lactanciaMat" >
                        Lactancia materna: 
                    </Label>
                    <Input id="lactanciaMat" name="lactanciaMat" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "ablactancia" >
                        Ablactancia: 
                    </Label>
                    <Input id="ablactancia" name="ablactancia" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "desarrollopsicomotriz" >
                        Desarrollo psicomotriz: 
                    </Label>
                    <Input id="desarrollopsicomotriz" name="desarrollopsicomotriz" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "menarquia" >
                        Menarquia: 
                    </Label>
                    <Input id="menarquia" name="menarquia" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "inicioRelaSex" >
                        Inicio relaciones sexuales: 
                    </Label>
                    <Input id="inicioRelaSex" name="inicioRelaSex" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "regimencatamenal" >
                        Regimen catamenal: 
                    </Label>
                    <Input id="regimencatamenal" name="regimencatamenal" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "gestacion" >
                        Gestacion: 
                    </Label>
                    <Input id="gestacion" name="gestacion" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "usoanticonceptivo" >
                        Uso anticonceptivos: 
                    </Label>
                    <Input id="usoanticonceptivo" name="usoanticonceptivo" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "ultimoparto" >
                        Ultimo parto: 
                    </Label>
                    <Input id="ultimoparto" name="ultimoparto" onChange={(e)=>{this.handleChange(e)}}/> 

                    
                    <h5>Imnunizaciones</h5>
                    {this.state.inmunizaciones.length>0?  (<Row>
                        <Col>
                            Nombre:
                        </Col>
                        <Col>
                            Fecha:
                        </Col>
                    </Row>
                    ): null}
                    <div>
                        {this.state.inmunizaciones.map(inmunizaciones=>{
                            return(
                                <Row> 
                                    <Col>
                                    <FormGroup>
                                    
                                    <Input id="nombreinmu" name="nombreinmu" onChange={(e)=>{this.handleChange(e)}}/> 
                                    </FormGroup>
                                    </Col>
                                    <Col>
                                    <FormGroup>
                                    
                                    <Input id="fechainmu" type="date" name="fechainmu" onChange={(e)=>{this.handleChange(e)}}/> 
                                    </FormGroup>
                                    </Col>
                                </Row>
                            );
                        })}
                        <Button color="success" onClick={this.agregarInmunizacion}>Añadir</Button>
                    </div>

                    <p></p>
                    {/* agregar tabla igual a la de familiar */}

                    <h1>Antecedentes Patologicos</h1>
                    <p></p>
                    <Label for= "enfermedadinfancia" >
                        Enfermedades de la infancia: 
                    </Label>
                    <Input id="enfermedadinfancia" name="enfermedadinfancia" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "enfermedadhereditaria" >
                        Enfermedades Hereditaria: 
                    </Label>
                    <Input id="enfermedadhereditaria" name="enfermedadhereditaria" placeholder="Enfermedades Hereditaria" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "antecedentesclinicos" >
                        Antecedentes clinicos: 
                    </Label>
                    <Input id="antecedentesclinicos" name="antecedentesclinicos" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "alergias" >
                        Alergias: 
                    </Label>
                    <Input id="alergias" name="alergias" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "antecedentesquirurgicos" >
                        Antecedentes quirugicos: 
                    </Label>
                    <Input id="antecedentesquirurgicos" name="antecedentesquirurgicos" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "enfermedadesginecologicas" >
                        Enfermedades ginecologicas: 
                    </Label>
                    <Input id="enfermedadesginecologicas" name="enfermedadesginecologicas" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "transfusiones" >
                        Transfuciones: 
                    </Label>
                    <Input id="transfusiones" name="transfusiones" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <Label for= "habitosnocivos" >
                        Habitos nocivos: 
                    </Label>
                    <Input id="habitosnocivos" name="habitosnocivos" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <h4>Antecedentes familiares</h4>
{/*         
                     <label for="test">Label:       </label>
                     <Input name="test" id="test" type="text" style="width:2"/>--class=" input input-sm"

                     <div class="container">
                         <Label for="test">Label:            </Label>
                         <span><input name="test" id="test" type="text" /></span>
                     </div>


                     <Label for="zip" >Zip Code</Label>
                     <Input type="text" id="zip" name="user_zip"/> */}
                     <Row>
                    <Col sm={{ size: 12}} md={{ size: 2, offset: 5 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                    <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning">Editar </Button> </Col>
                    <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuaryGuardar}  block  color="success"> Guardar y Continuar </Button>  </Col>  
                    </Row>
                </TabPane>  
                
                <TabPane tabId="4">
                    <Label for= "estadogeneral" >
                        Estado general: 
                    </Label>
                    <Input id="estadogeneral" name="estadogeneral" onChange={(e)=>{this.handleChange(e)}}/> 

                    <h4>Funciones vitales</h4>
                    <Label for= "frecuenciacardiaca" >
                        Frecuencia cardiaca: 
                    </Label>
                    <Input id="frecuenciacardiaca" name="frecuenciacardiaca" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "imc" >
                        IMC: 
                    </Label>
                    <Input id="imc" name="imc" onChange={(e)=>{this.handleChange(e)}}/> 

                    <Label for= "peso/edad" >
                        Peso/edad: 
                    </Label>
                    <Input id="peso/edad" name="peso/edad" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "talla/edad" >
                        Talla/edad: 
                    </Label>
                    <Input id="talla/edad" name="talla/edad" onChange={(e)=>{this.handleChange(e)}}/> 

                     <h4>Tejido celular subcutaneo</h4>
                     <Label for= "cabeza" >
                        Cabeza: 
                    </Label>
                    <Input id="cabeza" name="cabeza" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "ojos" >
                        Ojos: 
                    </Label>
                    <Input id="ojos" name="ojos" onChange={(e)=>{this.handleChange(e)}}/> 
                    
                    <h4>Aparato respiratorio</h4>
                    <Label for= "palpitacion" >
                        Palpitacion: 
                    </Label>
                    <Input id="palpitacion" name="palpitacion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "percusion" >
                        Percusion: 
                    </Label>
                    <Input id="percusion" name="percusion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "auscultacion" >
                        Auscultacion: 
                    </Label>
                    <Input id="auscultacion" name="auscultacion" onChange={(e)=>{this.handleChange(e)}}/> 

                    <h4>Aparato cardiovascular</h4>
                    <Label for= "palpitacion" >
                        Palpitacion: 
                    </Label>
                    <Input id="palpitacion" name="palpitacion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "percusion" >
                        Percusion: 
                    </Label>
                    <Input id="percusion" name="percusion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "auscultacion" >
                        Auscultacion: 
                    </Label>
                    <Input id="auscultacion" name="auscultacion" onChange={(e)=>{this.handleChange(e)}}/>

                    <h4>Abdomen</h4>
                    <p></p>
                    <Label for= "palpitacion" >
                        Palpitacion: 
                    </Label>
                    <Input id="palpitacion" name="palpitacion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "percusion" >
                        Percusion: 
                    </Label>
                    <Input id="percusion" name="percusion" onChange={(e)=>{this.handleChange(e)}}/> 
                    <Label for= "auscultacion" >
                        Auscultacion: 
                    </Label>
                    <Input id="auscultacion" name="auscultacion" onChange={(e)=>{this.handleChange(e)}}/>
                    
                </TabPane>

                <TabPane tabId="5">
                    <h3>Diagnostico Presuntivo</h3>
                    <Table bordered> 
                        <thead>
                            <tr>
                                <th colspan="1" class= "text-center"   >N° </th> 
                                <th colspan="1" class= "text-center"   >Diagnostico Presuntivo</th>
                                <th colspan="1" class= "text-center"   >COD CIE-10</th> 
                            </tr>
                        </thead> 
                        
                        <tbody>

                            <tr>
                                <td >1 </td>
                                <td ><Input id="t1valor1"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                                <td ><Input id="t1valor2"   type="number" onChange={(e)=>{this.handleChange(e)}}/> </td>
                            </tr>
                        </tbody>
                    </Table>
                    <Button>Buscar resultados de diagnosticos</Button>
                </TabPane>

                <TabPane tabId="6">
                    <Label for= "estadogeneral" >
                        Tratamiento: 
                    </Label>
                    <Input id="estadogeneral" name="estadogeneral" onChange={(e)=>{this.handleChange(e)}}/> 
                    {/* Indicaciones terapéuticas: dieta, cuidados de enfermería y de otros
                        profesionales que sean considerados necesarios, medicamentos
                        (consignando presentación, dosis, frecuencia y vía de administración). */}
                </TabPane>

                <TabPane tabId="7">
                    <h4>Plan de trabajo</h4>
                    <text>Busque los examenes de ayuda diagnostica: Procedimiento medicos,
                        quirurgicos e interconsultas.<p></p>  Luego los podra visualizar en la tabla final</text>
                    <p></p>
                    <Button color="info">Buscar examenes</Button>
                    <p></p>
                        <Modal isOpen={this.state.modalDiagnosticoImagenes}>
                            <ModalHeader>
                                Busqueda de examenes de ayuda diagnostica
                            </ModalHeader>
                            <ModalBody>
                            <FormGroup tag="fieldset">
                                <Label > En el siguiente cuadro proceda por tipo o nombre de examen </Label>
                            </FormGroup>

                            <FormGroup>
                                <InputGroup>
                                    <Input />
                                    <InputGroupAddon addonType="append"><Button>Buscar</Button></InputGroupAddon>
                                </InputGroup> 
                            </FormGroup>

                            <div>
                                <b>Lista de examenes escogidos</b>
                                <ul>
                                
                                </ul>
                            </div>
                        
                            <Col sm={{ size: 12}} md={{ size: 4, offset: 3 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Guardar </Button>  </Col>
                            </ModalBody>
                        </Modal>
                    <Table>
                    <thead>
                    <tr>
                        <th class= "text-center " bgcolor="#2294f2"  >Examen</th>
                        <th class= "text-center" bgcolor="#2294f2">Fecha</th>
                        <th  class= "text-center" bgcolor="#2294f2"   >Estado</th> 
                    </tr>
                </thead> 
                    </Table>
                </TabPane>

                        
            </TabContent>
            
          </Card>
        );
    }
} 

export default ConsultaExterna;


