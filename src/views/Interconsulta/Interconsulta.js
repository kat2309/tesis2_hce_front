import React from "react";
import { Nav, Progress, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody,Modal} from 'reactstrap';
import {imprimir} from "../../utils/generales.js";

class Interconsulta extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            tipo:"",
            descripInterconsulta:"",
            comentariosysug:"",
         
            // flag:{
                cancelarForm:false,
                provinciadisab:true,
                distritodisab:true,
            // }
            flag_mostrarOtro:false,
            porcCompletado:0,
        };
    };
    
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        if (id==="tipo" && value==="Otro"){
            await this.setState({
                flag_mostrarOtro:true})
            ;
            console.log("hola se ha impreso");
            // <Input id="otro"  onChange={(e)=>{this.handleChange(e)}}/> 
        }
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+33.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-33.3});
        
        await this.setState({
          [ id ]: value,
        });
    }
    cancelarFormulario = async()=> {
       imprimir("Cancelar");

       await this.setState({
           cancelarForm:true,
        //    flag: { ...this.state.flag, cancelarForm: true}
       });
       
       setTimeout(()=>this.setState({
            cancelarForm:false,
        }), 3000)
       
    }
    continuarSinGuardarFormulario = ()=> {
        
    }
    render(){
        return (
            <Card body>
                <h3 class="mb-3"> Interconsulta</h3>
                <div class="text-center">{this.state.porcCompletado}%</div>
                <Progress value={this.state.porcCompletado} /> 
                <Form>
                    <FormGroup>
                        <Label for= "tipo">
                            Tipo de la interconsulta: 
                        </Label>
                        <Input type="select" name="select" id="tipo" onChange={(e)=>{this.handleChange(e)}} >
                            <option> Diagnostico</option>
                            <option> Manejo Terapeutico</option>
                            <option> Procedimiento</option>
                            <option> Transferencia</option>
                            <option> Alta</option>
                            <option> Otro </option> 
                            
                            
                        </Input>
                    { this.state.flag_mostrarOtro ?
                                (<Input id="otro"  onChange={(e)=>{this.handleChange(e)}}/> ):null
                    }
                    </FormGroup>
                  
                    <FormGroup>
                        <Label for="descripInterconsulta">Descripcion de la interconsulta: </Label>
                        <Input type="textarea" name="descripInterconsulta" id="descripInterconsulta" onChange={(e)=>{this.handleChange(e)}} />
                    </FormGroup>
                    <FormGroup></FormGroup>

                    <FormGroup>
                        <Label for="comentariosysug"> Comentarios y sugerencias: </Label>
                        <Input type="textarea" name="comentariosysug" id="comentariosysug" onChange={(e)=>{this.handleChange(e)}} />
                    </FormGroup>
                    <FormGroup></FormGroup>
                    
                </Form>
                <Row>
                <Col sm={{ size: 12}} md={{ size: 2, offset: 8 }}>  <Button onClick={this.cancelarFormulario} block   color="danger"> Cancelar </Button>  </Col>  
                {/* <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning"> Continuar sin guardar </Button> </Col> */}
                <Col sm={{ size: 12}} md={{ size: 2 }}>   <Button onClick={this.continuarcancelarFormulario}  block  color="success"> Guardar </Button>  </Col>  
                </Row>
                <Modal centered isOpen = {this.state.cancelarForm}>
                    <ModalBody>
                        Se cancelo satisfactoriamente :)
                    </ModalBody>
                </Modal>
            </Card>
            
        );
    }
} 

export default Interconsulta;