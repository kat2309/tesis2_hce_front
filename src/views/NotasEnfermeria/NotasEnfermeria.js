import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,FormGroup, Spinner, Form} from 'reactstrap';
    import classnames from 'classnames';

class NotasEnfermeria extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            
            estadocivil:"",
            gradoinst:"",
            ocupacion:"",
            condicionocupacion:"",
            familiares:[],
            porcCompletado:0,
            fechahoy:"",
            horahoy:"",
            activeTab: '1',
            tratamientoAplicado:"",
            
        };
    };
    agregarFamiliar=()=>{
        let hora=new Date();
        let horaString1= hora.getHours()+":"+hora.getMinutes()+":"+hora.getSeconds();
        this.setState({
            familiares:this.state.familiares.concat({horahoy:horaString1}),
        })
    }
   
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+8.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-8.3});
        
        await this.setState({
          [ id ]: value,
        });
    }
    toggle=(tab)=> {
        // console.log("el tab es: ", tab)
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }
    componentDidMount(){
        let hoy = new Date();
        let hoyString = hoy.getDate() + "/" + (hoy.getMonth()+1)+"/"+hoy.getFullYear();
        
        let hora=new Date();
        let horaString= hora.getHours()+":"+hora.getMinutes()+":"+hora.getSeconds();
   
        this.setState({
            fechahoy:hoyString,
            horahoy:horaString,
        })
        this.agregarFamiliar();

    }
    continuaryGuardar=()=>{
        console.log(parseInt(this.state.activeTab));
      this.setState({
          activeTab: ( parseInt(this.state.activeTab)+1).toString()
        });
    }
    render(){
        return (
            <Card body>
                <h2 align="center"> Notas de Enfermeria </h2>
            <div>
            <img className="imagen-enfermera "  src="http://cliparts.co/cliparts/j5i/Rr8/j5iRr8eca.png"/>
            </div>
            <Nav tabs>
            
            <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '1' })}  onClick={() => { this.toggle('1'); }}  active >Nota de Ingreso</NavLink>
            </NavItem>
            
            <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >Evolucion</NavLink>
            </NavItem>
            </Nav>

            <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">

            
            {/* <Form>
                <FormGroup> */}
                    
                    <h5>  Notas de Ingreso: </h5>
                    <div> <b> Fecha: {this.state.fechahoy} &nbsp;&nbsp;&nbsp;&nbsp; Hora: {this.state.horahoy} </b>  </div>
                    {/* <div> Hora: {this.state.horahoy} </div> */}
                    <p></p>
                    <div> 
                        <Label style={{fontWeight: 'bold'}} for= "formaIngreso" >
                            Forma de Ingreso: 
                        </Label>
                        <Input type="textarea" id="formaIngreso" name="formaIngreso" placeholder="Forma de Ingreso" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "condicionPaciente" >
                            Condicion del paciente: 
                        </Label>
                        <Input type="textarea" id="condicionPaciente" name="condicionPaciente" placeholder="Condicion del paciente" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "funcionesVitales" >
                            Funciones vitales: 
                        </Label>
                        <Input type="textarea" id="funcionesVitales" name="funcionesVitales" placeholder="Funciones vitales" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "funcionesBiologicas" >
                            Funciones biologicas: 
                        </Label>
                        <Input type="textarea" id="funcionesBiologicas" name="funcionesBiologicas" placeholder="Funciones biologica" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "estadoGeneral" >
                            Estado General: 
                        </Label>
                        <Input type="textarea" id="estadoGeneral" name="estadoGeneral" placeholder="Estado General" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                    </div>
                {/* </FormGroup> */}
                <Row>
                <Col sm={{ size: 12}} md={{ size: 2, offset: 7 }}>  <Button onClick={this.continuarSinGuardarFormulario}  block  color="warning"> Editar </Button> </Col>
                <Col sm={{ size: 12}} md={{ size: 3 }}>   <Button onClick={this.continuaryGuardar}  block  color="success"> Guardar y Continuar </Button>  </Col>
                  
                </Row>

            </TabPane>
            
            <TabPane tabId="2">
           
                {this.state.familiares.map(familiar=>{
                   
                    return(
                        <Form> 
                        <FormGroup>
                            <div> <b> Fecha: {this.state.fechahoy} &nbsp;&nbsp;&nbsp;&nbsp; Hora: {familiar.horahoy} </b>
                            </div>
                            <Label for="signosSintomas" style={{fontWeight: 'bold'}}> 
                                Signos y sintomas: 
                            </Label>
                            <Input type="textarea" id="signosSintomas" name="signosSintomas" placeholder="Detalle los signos y sintomas que padece el paciente" onChange={(e)=>{this.handleChange(e)}} required/>
                        </FormGroup>

                        <FormGroup>
                    
                            <Label  for= "tratamientoAplicado" style={{fontWeight: 'bold'}} >
                            
                                Tratamiento aplicado: 
                            </Label>
                            <Input type="textarea" id="tratamientoAplicado" name="tratamientoAplicado" placeholder="Coloque el tratamiento aplicado" onChange={(e)=>{this.handleChange(e)}} required/> 
                        
                        </FormGroup>
                            <Row>
                                {/* <Col sm={{ size: 12}} md={{ size: 3, offset: 0 }}>  <Button onClick={this.agregarFamiliar}  color="warning"> Agregar Nota </Button> </Col> */}
                                <Col sm={{ size: 12}} md={{ size: 2 , offset: 10 }}>   <Button onClick={this.continuarcancelarFormulario}  color="success"> Guardar </Button>  </Col>
                            </Row>
                        </Form>
                        
                    )
                })}
            <Row> 
            <Col sm={{ size: 12}} md={{ size: 3, offset: 0 }}>  <Button onClick={this.agregarFamiliar}  color="warning"> Agregar Nota </Button> </Col>
            </Row>
            </TabPane>
        

            </TabContent>
       
            
            </Card>
          
        );
    }
} 

export default NotasEnfermeria;