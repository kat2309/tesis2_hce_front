import React from "react";
import { Badge,Nav, NavItem, TabContent, TabPane, NavLink, Card , Progress,  Button,Input,Label, Row, Col,ModalBody,Modal,InputGroup,  InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,FormGroup, Spinner} from 'reactstrap';
import classnames from 'classnames';

class Emergencia extends React.Component { 
    constructor(props) {
        super(props);  
        this.state = {
            
            estadocivil:"",
            gradoinst:"",
            ocupacion:"",
            condicionocupacion:"",
            familiares:[],
            porcCompletado:0,
            fechahoy:"",
            horahoy:"",
            activeTab: '1',
            tratamientoAplicado:"",
            
        };
    };
    agregarFamiliar=()=>{
        let hora=new Date();
        let horaString1= hora.getHours()+":"+hora.getMinutes()+":"+hora.getSeconds();
        this.setState({
            familiares:this.state.familiares.concat({horahoy:horaString1}),
        })
    }
   
    handleChange = async (event) => {
        const { target } = event;
        const { value,id } = target;
        
        if (value != null && this.state[id]!=value && this.state[id]==="") await this.setState({porcCompletado:this.state.porcCompletado+8.3});
        if (value === "") await this.setState({porcCompletado:this.state.porcCompletado-8.3});
        
        await this.setState({
          [ id ]: value,
        });
    }
    componentDidMount(){
        let hoy = new Date();
        let hoyString = hoy.getDate() + "/" + (hoy.getMonth()+1)+"/"+hoy.getFullYear();
        
        let hora=new Date();
        let horaString= hora.getHours()+":"+hora.getMinutes()+":"+hora.getSeconds();
   
        this.setState({
            fechahoy:hoyString,
            horahoy:horaString,
        })
        this.agregarFamiliar();

    }
    render(){
        return (
            <Card body>
                <h2 align="center"> Emergencia </h2>
                {/* <h5>  Notas de Ingreso: </h5> */}
                    <div> <b> Fecha: {this.state.fechahoy} &nbsp;&nbsp;&nbsp;&nbsp; Hora: {this.state.horahoy} </b>  </div>
                    {/* <div> Hora: {this.state.horahoy} </div> */}
                    <p></p>
                    <text> Nombre Completo: <p> </p>  </text>
                    <text> DNI:  <p> </p> </text>
                    <text> Edad:  <p> </p> </text>
                    <text> Grupo sanguineo:  <p> </p> </text>
                    <text> Genero:  <p> </p> </text>
                    <text> Numero de Triaje  <p> </p> </text>
                    <p></p>
                    <div> 
                        <Label> <b>Tipo de prioridad</b>  </Label>
                        <Row>
                            <Col>
                            <FormGroup check>
                            <Label check>
                                <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                    I
                    
                            </Label>
                            </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                        II
                                </Label>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                        III
                                </Label>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radioButtonBusqueda"/> {' '}
                                        IV
                                </Label>
                                </FormGroup>
                            </Col>
                        </Row>
                        <p></p>
                        <Label style={{fontWeight: 'bold'}} for= "motivoAtencion" >
                            Motivo de atencion: 
                        </Label>
                        <Input  id="motivoAtencion" name="motivoAtencion" placeholder="Motivo de atencion" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <Label style={{fontWeight: 'bold'}} for= "tiempoEnfermedad" >
                            Tiempo de enfermedad: 
                        </Label>
                        <Input  id="tiempoEnfermedad" name="tiempoEnfermedad" placeholder="Tiempo de enfermedad" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "sintomasPrincipales" >
                            Sintomas principales: 
                        </Label>
                        <Input  id="sintomasPrincipales" name="sintomasPrincipales" placeholder="Sintomas principales" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>
                        <Label style={{fontWeight: 'bold'}} for= "relato" >
                            Relato: 
                        </Label>
                        <Input type="textarea" id="relato" name="relato" placeholder="Relato" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>

                        <Button onClick={this.abrirModal}> Seleccionar Antecedentes </Button>
                        <Modal isOpen={this.state.modalDiagnosticoImagenes}></Modal>
                        <p></p>
                        <Label style={{fontWeight: 'bold'}} for= "hospitalizacionesPrevias" >
                            Hospitalizaciones previas: 
                        </Label>
                       
                        <Input  id="hospitalizacionesPrevias" name="hospitalizacionesPrevias" placeholder="Hospitalizaciones previas" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>

                        <Label style={{fontWeight: 'bold'}} for= "medicacionHabitual" >
                            Medicacion habitual: 
                        </Label>
                        <Input  id="medicacionHabitual" name="medicacionHabitual" placeholder="Medicacion habitual" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>

                        <Button onClick={this.abrirModal}> Seleccionar Medicamentos </Button>
                        <Modal isOpen={this.state.modalDiagnosticoImagenes}></Modal>

                        <p></p>
                        <Label> <b>Estado basal:</b>  </Label>
                        <Row>
                            <Col>
                            <FormGroup check>
                            <Label check>
                                <Input type="radio" name="radioEstadobasal"/> {' '}
                                    Independiente
                    
                            </Label>
                            </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radioEstadobasal"/> {' '}
                                        Dependiente Parcial
                                </Label>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radioEstadobasal"/> {' '}
                                        Dependiente Total
                                </Label>
                                </FormGroup>
                            </Col>
                            
                        </Row>
                        <p></p>

                        <Label style={{fontWeight: 'bold'}} for= "examenfisico" >
                            Examen fisico: 
                        </Label>
                        <Input  id="examenfisico" name="examenfisico" placeholder="Examen fisico" onChange={(e)=>{this.handleChange(e)}} required/> 
                        <p> </p>

                    </div>
            </Card>
            );
        }
}
 
    
export default Emergencia;