import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Nav, Progress, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody,Modal,InputGroup,
    InputGroupAddon,TabContent, TabPane,Table, CardBody} from 'reactstrap';

class CELista extends Component {
    render() {
      return (
          
          <Card>
              <p></p>
              <h2 class="mb-3" align="center"> Lista de Pacientes por atender </h2>
              <p></p>
                <CardBody>
                    <TabContent> 
                        <TabPane>
                            <Table bordered >
                                <thead>
                                    <tr>
                                        <th class= "text-center" bgcolor="#eb9694" >Hora de atencion (hrs) </th>
                                        <th class= "text-center" bgcolor="#eb9694" > DNI Paciente</th>
                                        <th colspan="1" class= "text-center" bgcolor="#eb9694"  >Nombre del Paciente</th>
                                        <th colspan="1" class= "text-center" bgcolor="#eb9694"  > Estado</th>
                                        
                                    </tr>
                                </thead> 
                               
                                <tbody>
                                    <tr>
                                        <td ><text>   08:00  </text> </td>
                                        <td > <text> 12345678  </text> </td>
                                        <td ><text>  Katherine Ruiz Ferraza</text> </td>
                                        <td ><Button  href="http://localhost:3000/#/Inicio" color="link">Por atender</Button> </td>
                                    </tr>
                                    
                                    <tr>
                                        <td ><text>   09:00 </text> </td>
                                        <td > <text> 34231342  </text> </td>
                                        <td ><text>  Kiara Chambilla Quispe</text> </td>
                                         {/* el estado de canelado va a estar como no se puede entrar sin link y en gris */}
                                        <td ><Button  color="link">Cancelado</Button> </td> 
                                         
                                    </tr>
                                    <tr>
                                        <td ><text>   10:30 </text> </td>
                                        <td > <text> 35531342  </text> </td>
                                        <td ><text>  Diana Ruiz Froz</text> </td>
                                        <td ><Button href="http://localhost:3000/#/Inicio"  color="link">Por atender</Button> </td>
                                    </tr>
                                    <tr>
                                        <td ><text>   13:15 </text> </td>
                                        <td > <text> 43434545  </text> </td>
                                        <td ><text>  Carolina Rios Espinoza</text> </td>
                                        <td ><Button href="http://localhost:3000/#/Inicio"  color="link">Nuevo/ Por atender/ Triaje</Button> </td>
                                    </tr>
                                  
                                </tbody>
                            </Table>
                            <Row>
                                <Col sm={{ size: 12}} md={{ size: 3,offset:8}}>  <Button onClick={this.continuarSinGuardarFormulario}  block  color="danger"> Salir </Button> </Col>
                              
                            
                            </Row>
                        </TabPane>
                    </TabContent>
                    {/* demostrar de todas maneras que si pasa mas de 30 min de espera no sera atendido y cambiara de estado a cancelado */}
                </CardBody>
          </Card>
       
        
      )
    }
}

export default CELista;