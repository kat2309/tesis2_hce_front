import React, { Component } from 'react';
import gif from "../../assets/img/brand/huelladigital.gif"
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class LoginH extends Component {
  render() {
    return (
      // <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Ingresa a tu cuenta</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Usuario" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Contraseña" autoComplete="current-password" />
                      </InputGroup>
                      {/* agregar git */}
                      <div>
                        <label> Ingrese su huella digital (Haga click en la imagen) </label>
                        <img src={gif}/>
                        {/* C:\Users\kathe\Desktop\coreui-free-react-admin-template-master\coreui-free-react-admin-template-master\src\assets\img\brand */}
                      </div>
                      <Row>
                        <Col xs="6">
                          <Button href="http://localhost:3000/#/CELista" color="primary" className="px-4">Ingresar</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">¿Olvidaste tu constraseña?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                
              </CardGroup>
            </Col>
          </Row>
        </Container>
      // </div>
    );
  }
}

export default LoginH;
