import React from "react";
import { Nav, Progress, Card, Button,Form, FormGroup,Input,Label, Row, Col,ModalBody,Modal,Table} from 'reactstrap';
import {imprimir} from "../../utils/generales.js";

class InformeAlta extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            nombres: "",
            apellidos:"",
            telefono:"",
            ocupacion:"",
            genero:"",
            domicilio:"",
            grupofactor:"",
            lugarnacimiento:"",
            provincia:"",
            distrito:"",
            idioma:"",
            // flag:{
                cancelarForm:false,
                provinciadisab:true,
                distritodisab:true,
            // }
            porcCompletado:0,
        };
    };
    render(){
        return(
            <Form> 
                <FormGroup>
                    Nombre del Paciente: Katherine Ruiz <tab> </tab>
                    Pabellon: E-2
                </FormGroup>
                <FormGroup>
                    <Table bordered>
                        <text> Tratamiento: Medicamentos</text>
                        <thead>
                        <th class= "text-center" bgcolor="#30d7ec"  >Farmaco</th>
                        <th class= "text-center" bgcolor="#30d7ec"   >Presentacion</th> 
                        <th class= "text-center" bgcolor="#30d7ec"  >Dosis</th>
                        <th class= "text-center" bgcolor="#30d7ec"   >Via</th> 
                        <th class= "text-center" bgcolor="#30d7ec"  >Frecuencia</th>
                        <th class= "text-center" bgcolor="#30d7ec"   >Duracion</th> 
                        </thead>
                    </Table>
                </FormGroup>
                <FormGroup>
                <Label for="recomendaciones" style={{fontWeight: 'bold'}}> 
                    Recomendaciones: 
                </Label>
                <Input type="textarea" id="recomendaciones" name="recomendaciones" placeholder="Recomendaciones" onChange={(e)=>{this.handleChange(e)}} required/>
                </FormGroup>
            </Form>
        )
    }
}

export default InformeAlta;